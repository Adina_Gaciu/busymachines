<?php


namespace App\Controller;

use App\Repository\ProductRepository;
use App\HTTP\ProductProcessor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;


/**
 * Class GorestApiController
 * @package App\Controller
 */
class GorestApiController extends AbstractController

{
    /**
     * @Route("/api/getProducts", name="api_get_products")
     * @param ProductProcessor $productProcessor
     * @param ProductRepository $productRepository
     */
    public function getProducts(ProductProcessor $productProcessor, ProductRepository $productRepository ,SerializerInterface $serializer)
     {
          $data =$productProcessor->processedProducts($productRepository);

          if($data){
              $products = $productRepository->findAll();
              $jsonString = $serializer->serialize($products, 'json');
         dd($jsonString);

        }


     }
}