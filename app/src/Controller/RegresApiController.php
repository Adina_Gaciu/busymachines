<?php


namespace App\Controller;


use App\HTTP\UserProcessor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;



class RegresApiController extends AbstractController
{
    /**
     *
     *
     * @Route("/api/getUsers", name="api_get_users")
     * @param UserProcessor $userProcessor
     * @param UserRepository $userRepository
     */
    public function getusers(UserProcessor $userProcessor,  UserRepository $userRepository,SerializerInterface $serializer)
    {
        $data = $userProcessor->processedUsers($userRepository);

        if ($data) {
            $products = $userRepository->findAll();
            $jsonString = $serializer->serialize($products, 'json');
            dd($jsonString);
        }
    }
}