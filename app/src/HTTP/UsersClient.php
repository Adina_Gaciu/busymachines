<?php


namespace App\HTTP;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class UsersClient
 * @package App\Service
 */
class UsersClient
{
    /**
     * @var HttpClientInterface
     */
    private $client;


    /**
     * UsersClient constructor.
     * @param HttpClientInterface $client
     */
    public function __construct( HttpClientInterface $client )
    {
        $this->client =$client;

    }

    /**
     * @return mixed
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getUsers(){

        $response = $this->client->request(
            'GET',
            'https://reqres.in/api/users?page=2'
        );

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $data =  $serializer->decode($response->getContent(), 'json');
        return $data['data'];
    }

}