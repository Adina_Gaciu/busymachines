<?php


namespace App\HTTP;


use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class ProductProcessor
{
    /**
     * @var ProductClient
     */
    private $productClient;


    /**
     * ProductProcessor constructor.
     * @param ProductClient $productClient
     */
    public function __construct( ProductClient $productClient)
    {
        $this->productClient = $productClient;
    }
    public function processedProducts(ProductRepository $productRepository): bool
    {
        try {
            $productsRaw = $this->productClient->getProducts();
            $productsProcessed = $this->processProducts($productsRaw);
            foreach ($productsProcessed as $item){
                if(!$productRepository->findOneBySomeField($item['name']))
                {
                    $product = new Product();
                    $product->setName($item['name']);
                    $product->setPrice($item['price']);
                    $product->setDiscountAmount($item['discount_amount']);
                    $product->setNewPrice($item['new_price']);
                    $productRepository->addProduct($product);
                }


            }
        } catch (ClientExceptionInterface | TransportExceptionInterface | ServerExceptionInterface | RedirectionExceptionInterface $e) {

            return false;
        }

        return true;

    }

    /**
     * @param $products
     * @return array
     */
    private function processProducts($products): array
    {
        $processedProducts =[];
        foreach($products as $product){
            $product['new_price']= $product['price'] -$product['discount_amount'];
            array_push( $processedProducts,$product);
        }
        return $processedProducts;

    }

}