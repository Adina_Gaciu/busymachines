<?php


namespace App\HTTP;


use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class UserProcessor
 * @package App\Service
 */
class UserProcessor
{
    /**
     * @var UsersClient
     */
    private $usersClient;

    /**
     * UserProcessor constructor.
     * @param UsersClient $usersClient
     */
    public function __construct(UsersClient $usersClient)
    {
        $this->usersClient = $usersClient;
    }

    public function getProcessedUsers(UserRepository $userRepository): bool
    {
        try {
            $usersRaw = $this->usersClient->getUsers();
            $usersProcessed = $this->processUsers($usersRaw);
            foreach ($usersProcessed as $item){
                if(!$userRepository->findOneBySomeField($item['name']))
                {
                    $user = new User();
                    $user->setEmail($item['email']);
                    $user->setFirstName($item['first_name']);
                    $user->setLastName($item['last_name']);
                    $user->setAvatar($item['avatar']);
                    $userRepository->addUser($user);
                }


            }

        } catch (ClientExceptionInterface | TransportExceptionInterface | ServerExceptionInterface | RedirectionExceptionInterface $e) {

            return false;
        }

        return true;

    }

    /**
     * @param $users
     * @return array
     */
    private function processUsers($users): array
    {
      $processedUsers =[];
      foreach($users as $user){
          $user['last_name']= strrev($user['last_name']);
         array_push( $processedUsers,$user  );
      }
        return $processedUsers;

    }
}